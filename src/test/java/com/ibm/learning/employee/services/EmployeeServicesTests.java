package com.ibm.learning.employee.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ibm.learning.employee.dao.EmployeeDao;
import com.ibm.learning.employee.entities.Employee;

@RunWith(SpringJUnit4ClassRunner.class)
public class EmployeeServicesTests {
	
	@Mock
	private EmployeeDao employeeDao;
	
	@InjectMocks
	private EmployeeServices employeeServices;
	
	@Before
	public void setup(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testCreateEmployee(){
		Employee employee1 = new Employee("akbar","akbar@gmail.com");
		when(employeeDao.save(employee1)).thenReturn(employee1);
		Employee employee = employeeServices.createEmployee(employee1);
		assertEquals(employee.getFirstName(), employee1.getFirstName());
	}
	
	
	@Test
	public void testGetListOfEmployees(){
		List<Employee> employeeList = new ArrayList<Employee>();
		//Employee employee1 = new Employee("akbar","akbar@gmail.com");
		employeeList.add(new Employee("akbar","akbar@gmail.com"));
		employeeList.add(new Employee("akbar","akbar@gmail.com"));
		employeeList.add(new Employee("akbar","akbar@gmail.com"));
		when(employeeDao.findAll()).thenReturn(employeeList);
		
		List<Employee> result = employeeServices.getListOfEmployees();
		assertEquals(3, result.size());
	}
	
	@Test
	public void testEmployeeById(){
		
		Employee emp = new Employee("Akbar", "akbar@gmail.com");
		Optional<Employee> employee1 = Optional.of(emp);
		when(employeeDao.findById(1)).thenReturn(employee1);
		Employee emp1 = employeeServices.getEmployeeById(1);
		assertEquals("Akbar", emp1.getFirstName());
		assertEquals("akbar@gmail.com", emp1.getMiddleName());
	}
		

}
