package com.ibm.learning.employee.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ibm.learning.employee.entities.Employee;
import com.ibm.learning.employee.services.EmployeeServices;

@RestController
@RequestMapping("/api")
public class EmployeeController {

	@Autowired
	private EmployeeServices employeeServices;
	
	@PostMapping("/createEmployee")
	public Employee createEmployee(@RequestBody Employee employee) {
		return employeeServices.createEmployee(employee);
	}

	@GetMapping("/getListOfEmployees")
	public List<Employee> getListOfEmployees() {
		return employeeServices.getListOfEmployees();
	}

	@GetMapping("/getEmployeetById/{id}")
	public Employee getEmployeetById(@PathVariable Integer id) {
		return employeeServices.getEmployeeById(id);
	}

	@PutMapping("/updateEmployee")
	public Employee updateEmployee(@RequestBody Employee employee) {
		return employeeServices.updateEmployee(employee);
	}
	
	@DeleteMapping("/deleteEmployeebyId/{id}")
	public void deleteEmployeebyId(@PathVariable Integer id) {
		employeeServices.deleteEmployeebyId(id);
	}
	
}
