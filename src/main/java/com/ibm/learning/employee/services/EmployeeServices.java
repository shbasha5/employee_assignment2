package com.ibm.learning.employee.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ibm.learning.employee.dao.DepartmentDao;
import com.ibm.learning.employee.dao.EmployeeDao;
import com.ibm.learning.employee.entities.Department;
import com.ibm.learning.employee.entities.Employee;

@Service
public class EmployeeServices {

	@Autowired
	EmployeeDao employeeDao;
	
	@Autowired
	DepartmentDao departmentDao;
	
	public Employee createEmployee(Employee employee) {
		return employeeDao.save(employee);
	}

	public List<Employee> getListOfEmployees() {

		Iterable<Employee> allEmployees = employeeDao.findAll();
		List<Employee> employeeList = new ArrayList<>();
		allEmployees.forEach(employeeList::add);
		return employeeList;
	}

	public Employee getEmployeeById(Integer id) {
		return employeeDao.findById(id).orElse(null);

	}

	public Employee updateEmployee(Employee employee) {

		Employee employeeUpdated = employeeDao.findById(employee.getEmployeeId()).orElse(new Employee());

		employeeUpdated = employee;
		return employeeDao.save(employeeUpdated);
	}

	public void deleteEmployeebyId(Integer id) {
		employeeDao.deleteById(id);
	}
	
	public Department saveDepartment(Department dept) {
		return departmentDao.save(dept);
	}

	public void createDept( ) {	
		Department department = new Department();
		department.setId(1001);	
		department.setName("Devep");
		departmentDao.save(department);
	}
}
