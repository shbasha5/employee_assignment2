package com.ibm.learning.employee.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ibm.learning.employee.entities.Department;

@Repository
public interface DepartmentDao extends CrudRepository<Department, Integer>{

}

