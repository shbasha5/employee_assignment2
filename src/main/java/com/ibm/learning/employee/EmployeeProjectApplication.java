package com.ibm.learning.employee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.ibm.learning.employee.services.EmployeeServices;

@SpringBootApplication
public class EmployeeProjectApplication {

	
	public static void main(String[] args) {
		SpringApplication springApplication = new SpringApplication(EmployeeProjectApplication.class);
		ConfigurableApplicationContext applicationContext = springApplication.run(args);
		EmployeeServices employeeServices = applicationContext.getBean(EmployeeServices.class);
		employeeServices.createDept();
//		SpringApplication.run(EmployeeProjectApplication.class, args);		
	}
}
